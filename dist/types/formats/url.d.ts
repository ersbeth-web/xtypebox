/**
 * `[ajv-formats:deprecated]` A uniform resource locator as defined in [RFC 1738](https://www.rfc-editor.org/rfc/rfc1738)
 * @example `http://domain.com`
 */
export declare function IsUrl(value: string): boolean;
//# sourceMappingURL=url.d.ts.map