/**
 * `[ajv-formats]` Internet Email Address [RFC 5321, section 4.1.2.](http://tools.ietf.org/html/rfc5321#section-4.1.2)
 * @example `user@domain.com`
 */
export declare function IsEmail(value: string): boolean;
//# sourceMappingURL=email.d.ts.map