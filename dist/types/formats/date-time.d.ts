/**
 * `[ajv-formats]` ISO8601 DateTime
 * @example `2020-12-12T20:20:40+00:00`
 */
export declare function IsDateTime(value: string, strictTimeZone?: boolean): boolean;
//# sourceMappingURL=date-time.d.ts.map