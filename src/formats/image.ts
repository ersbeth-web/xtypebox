/**
 * Image data url
 */
export function IsImageDataUrl(value: string): boolean {
    const regexp = /^data:image/
    return regexp.test(value)
}