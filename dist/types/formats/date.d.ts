/**
 * `[ajv-formats]` ISO8601 Date component
 * @example `2020-12-12`
 */
export declare function IsDate(value: string): boolean;
//# sourceMappingURL=date.d.ts.map