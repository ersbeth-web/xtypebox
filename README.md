# xTypeBox

Additional utilities for [TypeBox](https://github.com/sinclairzx81/typebox).

## Installation
```  npm install @ersbeth/xtypebox```

## Usage

### String formats

`xTypeBox` provides additional string formats for validation. First you must configure them: 

```js
import { IsDate, IsTime, IsDateTime, IsEmail, IsUrl, IsImageDataUrl, IsUuid, IsIPv4, IsIPv6, } from "@ersbeth/xtypebox";

FormatRegistry.Set('date', IsDate)
FormatRegistry.Set('time', IsTime)
FormatRegistry.Set('date-time', IsDateTime)

FormatRegistry.Set('email', IsEmail)
FormatRegistry.Set('url', IsUrl)
FormatRegistry.Set('url-image', IsImageDataUrl)

FormatRegistry.Set('uuid', IsUuid)

FormatRegistry.Set('ipv4', IsIPv4)
FormatRegistry.Set('ipv6', IsIPv6)
```

Then you may use them in any typebox type:
```js
const Schema = Type.Object({
    url: Type.String({ format: "url" }),
    imageDataUrl: Type.String({ format: "url-image" }),
    email: Type.String({ format: "email" })
})
```