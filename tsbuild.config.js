export default {
    tsbuild: {
        file: 'src/index.ts',
        out: 'dist',
    },
    esbuild: {
        bundle: true,
        packages: 'external',
        platform: 'neutral',
        target: 'esnext',
        sourcemap: true,
    }
}
