/**
 * `[ajv-formats]` IPv6 address as defined in [RFC 2373, section 2.2](http://tools.ietf.org/html/rfc2373#section-2.2).
 * @example `2001:0db8:85a3:0000:0000:8a2e:0370:7334`
 */
export declare function IsIPv6(value: string): boolean;
//# sourceMappingURL=ipv6.d.ts.map