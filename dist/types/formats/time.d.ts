/**
 * `[ajv-formats]` ISO8601 Time component
 * @example `20:20:39+00:00`
 */
export declare function IsTime(value: string, strictTimeZone?: boolean): boolean;
//# sourceMappingURL=time.d.ts.map