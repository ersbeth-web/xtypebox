/**
 * `[ajv-formats]` IPv4 address according to dotted-quad ABNF syntax as defined in [RFC 2673, section 3.2](http://tools.ietf.org/html/rfc2673#section-3.2)
 * @example `192.168.0.1`
 */
export declare function IsIPv4(value: string): boolean;
//# sourceMappingURL=ipv4.d.ts.map